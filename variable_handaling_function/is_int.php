<?php

$values = array(23,"23", 23.5 , null , true, false);

foreach($values as $value)
{
    echo "is_int(";
    var_export($value);
    echo ") =";
    var_dump(is_int($value));
    echo "<br/>";
}

error_reporting(E_ALL);

$foo = NULL;

var_dump(is_null($inexistent), is_null($foo));


$var = '';

if(isset($var))
{
    echo "<br/><br/> Variable is set";
}
else{
    echo "<br/> Variable is not set";
}

$a = "test";

unset($a);

if(isset($a)){
    echo "<br/> Variable is set";
}
else{
    echo "<br/> Variable is not set";
}

?>


<?php

 echo "<pre>";
$b = array('a' => 'apple', 'b' => 'Banana', 'c' => array('x','y','z'));
print_r($b);

echo "</pre>"

?>


